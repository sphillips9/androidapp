package com.example.sean.testing;

import java.util.Map;

/**
 * Created by Sean on 2/13/2017.
 */

public class CollectionCriteraParse {
    private Map<String, String> collectionMap;
    private Map<String, String> valueMap;
    private String collection_key;
    private String collection_value;
    private String current_value;
    private String percent;
    private boolean collection_selected = false;

    public void setUPMap(){
        if(collectionMap.get("a_sort_check_selected") != null && collectionMap.get("a_sort_check_selected").equals("true")){
            collection_key = "A Sort#";
            collection_value = collectionMap.get("a_sort_check_count");
            current_value = valueMap.get("a_trigger_count");
            collection_selected = true;

            int temp_percent = Integer.parseInt(current_value) * 100 / Integer.parseInt(collection_value);
            percent = Integer.toString(temp_percent);
        }
        if(collectionMap.get("c_sort_check_selected") != null && collectionMap.get("c_sort_check_selected").equals("true")){
            collection_key = "C Sort#";
           collection_value = collectionMap.get("c_sort_check_count");
            current_value = valueMap.get("c_trigger_count");
            collection_selected = true;

            int temp_percent = Integer.parseInt(current_value) * 100 / Integer.parseInt(collection_value);
            percent = Integer.toString(temp_percent);
        }
        if(collectionMap.get("time_check_selected") != null && collectionMap.get("time_check_selected").equals("true")){
            collection_key = "Time";
            collection_value = collectionMap.get("time_check_count");
            current_value = valueMap.get("elapsed");
            collection_selected = true;

            int temp_percent = Integer.parseInt(current_value) * 100 / Integer.parseInt(collection_value);
            percent = Integer.toString(temp_percent);
        }
        if(collectionMap.get("count_check_selected") != null && collectionMap.get("count_check_selected").equals("true")){
            collection_key = "Count";
            collection_value = collectionMap.get("count_check_count");
            current_value = valueMap.get("total_cell_value");
            collection_selected = true;

            int temp_percent = Integer.parseInt(current_value) * 100 / Integer.parseInt(collection_value);
            percent = Integer.toString(temp_percent);
        }
        if(collectionMap.get("volume_check_selected") != null && collectionMap.get("volume_check_selected").equals("true")){
            collection_key = "Volume";
            collection_value = collectionMap.get("volume_check_count");
            current_value = valueMap.get("volume");
            collection_selected = true;

            double temp_current_value = Double.parseDouble(valueMap.get("a_trigger_count"));
            current_value = Integer.toString((int)temp_current_value);
            double temp_percent = Double.parseDouble(current_value) * 100 / Double.parseDouble(collection_value);
            percent = Integer.toString((int)temp_percent);
        }
    }

    public String getCollection_key(){
        return collection_key;
    }

    public String getCollection_value(){
        return collection_value;
    }

    public String getCurrent_value() { return current_value; }

    public String getPercent(){ return  percent; }

    public void setCollectionMap(Map<String, String> map){
        collectionMap =  map;
    }

    public void setValueMap(Map<String, String> map){ valueMap = map; }

    public boolean getColletionSelected(){
        return collection_selected;
    }
}
