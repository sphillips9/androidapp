package com.example.sean.testing;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class DeviceInfo extends AppCompatActivity {
    private TextView textView;
    private String authKey =  "";
    private String current_device = "";
    private RelativeLayout relativeLayout;
    private Toolbar bar;
    private ProgressDialog dialog;
    private JSONParse jsonParse;
    private final static int INTERVAL = 10000;
    private Handler handler;
    private CollectionCriteraParse collectionCriteraParse = new CollectionCriteraParse();

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_info);
        relativeLayout = (RelativeLayout) findViewById(R.id.device_info_page);

        Intent me = getIntent();
        current_device = me.getStringExtra("address");
        authKey = me.getStringExtra("authKey");

        bar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) findViewById(R.id.title_text)).setText(current_device);
        setSupportActionBar(bar);
        DoTask doTask = new DoTask();
        doTask.execute();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                DoTask doTask = new DoTask();
                doTask.execute();
            }
        };
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                DoTask doTask = new DoTask();
                doTask.execute();
                handler.postDelayed(this, INTERVAL);
            }
        },INTERVAL);
    }

    private void setUPCriteria(JSONParse jsonParse){
        collectionCriteraParse.setCollectionMap(jsonParse.CollectionCriteriaMap);
        collectionCriteraParse.setValueMap(jsonParse.CurrentValue);
        collectionCriteraParse.setUPMap();
        TextView key =  (TextView)findViewById(R.id.selected_collection_key);
        TextView value = (TextView)findViewById(R.id.selected_collection_value);
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressbar);
        TextView progresstext = (TextView) findViewById(R.id.progresstext);
        if(collectionCriteraParse.getColletionSelected()){
            Log.d("TRUE", collectionCriteraParse.getCollection_key());
            key.setText(collectionCriteraParse.getCollection_key());
            value.setText(collectionCriteraParse.getCurrent_value()+" / "+collectionCriteraParse.getCollection_value());
            progressBar.setProgress(Integer.parseInt(collectionCriteraParse.getPercent()));
            progresstext.setText(collectionCriteraParse.getPercent()+"%");
            Log.d("percent", collectionCriteraParse.getPercent());
        }else{
            Log.d("FALSE", "NONE");
            key.setText("No Selection : ");
            value.setText("No Data");
            progressBar.setProgress(100);
            progressBar.setProgress(0);
            progresstext.setText("0%");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            handler.removeMessages(0);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void processFinish(JSONParse jsonParse) {
        int textSize = 18;
        int padding = 30;
        int view_padding = 72;
        int view_height = findViewById(R.id.view_1).getHeight();
        int view_width = findViewById(R.id.view_1).getWidth();

        textView = (TextView) findViewById(R.id.info_stage_value);
        textView.setText(jsonParse.GUIStage + "    ");
        textView = (TextView) findViewById(R.id.info_totalevent_value);
        textView.setText(jsonParse.totalCellCount + "    ");
        textView = (TextView) findViewById(R.id.info_volume_value);
        textView.setText(jsonParse.Volume + "    ");
        textView = (TextView) findViewById(R.id.info_timeelapsed_value);
        textView.setText(jsonParse.TimeElapse + "    ");

        TableLayout table = (TableLayout)findViewById(R.id.gate_table);
        table.removeAllViews();
        if (jsonParse.Gates.size() == 0) {
            TableRow row = new TableRow(this);
            TextView newText = new TextView(this);
            newText.setText(jsonParse.gates);
            newText.setTextSize(textSize);
            newText.setPadding(0,padding,0, padding);
            newText.setTextColor(Color.WHITE);
            newText.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
            View view = new View(this);
            view.setBackgroundColor(Color.WHITE);
            view.setPadding(view_padding, 0,0,0);
            view.setMinimumHeight(view_height);
            view.setMinimumWidth(view_width);
            row.addView(newText);
            table.addView(row);
        } else {
            for(int i = 0; i < jsonParse.Gates.size(); i++){
                Object key = jsonParse.Gates.keySet().toArray()[i];
                Object value = jsonParse.Gates.get(key);

                TableRow row = new TableRow(this);
                row.setLayoutParams(new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                TextView keyText = new TextView(this);
                TextView valueText = new TextView(this);

                keyText.setText("     "+key.toString());
                keyText.setTextSize(textSize);
                keyText.setPadding(0,padding,0, padding);
                keyText.setTextColor(Color.WHITE);
                keyText.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
                keyText.setGravity(Gravity.LEFT);

                valueText.setText(value.toString()+"    ");
                valueText.setTextSize(textSize);
                valueText.setPadding(0,padding,0, padding);
                valueText.setTextColor(Color.WHITE);
                valueText.setGravity(Gravity.RIGHT);
                valueText.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);

                row.addView(keyText);
                row.addView(valueText);
                table.addView(row);

                if(i < jsonParse.Gates.size()-1){

                    TextView view_line = new TextView(this);
                    view_line.setHeight(view_height);
                    view_line.setWidth(view_width);
                    view_line.setBackgroundColor(Color.WHITE);
                    TableRow.LayoutParams params = new TableRow.LayoutParams(0,-2,1f);
                    params.setMargins(view_padding,0,0,0);
                    view_line.setLayoutParams(params);
                    TableRow outer = new TableRow(this);
                    outer.setGravity(Gravity.RIGHT);
                    outer.addView(view_line);
                    table.addView(outer);
                }
            }
        }
    }

    private class DoTask extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {
            if(jsonParse == null){
                jsonParse = new JSONParse(authKey, current_device, null);
            }else{
                jsonParse.jsonStart();
            }
            return null;
        }
    }
}
