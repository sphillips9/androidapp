package com.example.sean.testing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Sean on 1/26/2017.
 */

public class DeviceMain extends AppCompatActivity{
    private String authKey = "kendall";
    private Toolbar bar;
    private String current_device = "devices";
    private JSONParse jsonParse;
    private ProgressDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) findViewById(R.id.title_text)).setText("Select Device");
        setSupportActionBar(bar);

        DoTask task = new DoTask();
        task.execute();
    }

    public void processFinish(JSONParse jsonParse) {
        int textSize = 18;
        int padding = 35;
        int view_padding = 72;
        int view_height = 3;

        TableLayout table = (TableLayout) findViewById(R.id.main_table);
        for (int i = 0; i < jsonParse.address.length; i++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            TextView device_view = new TextView(this);
            device_view.setText("Wolf Device : " + jsonParse.address[i]);
            device_view.setPadding(0, padding, 0, padding);
            device_view.setTextColor(Color.WHITE);
            device_view.setTextSize(textSize);
            device_view.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
            device_view.setGravity(Gravity.LEFT);

            TableRow.LayoutParams param = new TableRow.LayoutParams(0, -2, 1f);
            param.setMargins(view_padding, 0, 0, 0);
            device_view.setLayoutParams(param);

            final Intent next = new Intent(DeviceMain.this, DeviceInfo.class);
            next.putExtra("address", jsonParse.address[i]);
            next.putExtra("authKey", authKey);
            device_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(next);
                }
            });
            row.addView(device_view);
            table.addView(row);

            if (i < jsonParse.address.length - 1) {

                TextView view_line = new TextView(this);
                view_line.setHeight(view_height);
                view_line.setBackgroundColor(Color.WHITE);
                TableRow.LayoutParams params = new TableRow.LayoutParams(0, -2, 1f);
                params.setMargins(view_padding, 0, 0, 0);
                view_line.setLayoutParams(params);
                TableRow outer = new TableRow(this);
                outer.setGravity(Gravity.RIGHT);
                outer.addView(view_line);
                table.addView(outer);
            }
        }
    }

        private class DoTask extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {
            if(jsonParse == null){
                jsonParse = new JSONParse(authKey, current_device, null);
            }else{
                jsonParse.jsonStart();
            }
            return null;
        }
    }
}
