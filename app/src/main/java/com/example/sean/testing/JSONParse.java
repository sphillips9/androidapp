package com.example.sean.testing;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Sean on 2/3/2017.
 */

public class JSONParse {
    private String authKey= "";
    private String currentDevice = "";
    private String endPoint = "";
    private String baseURL = "https://nanocellect.herokuapp.com/";
    private String url;

    public String[] address;

    public int elapse = 0;
    public int totalCellCount = 0;
    public int ATriggerCount = 0;
    public int AImpedanceCount = 0;
    public double AEfficiency = 0.0;
    public int CTriggerCount = 0;
    public int CImpedanceCount = 0;
    public double CEfficiency = 0.0;
    public int BImpedanceCount = 0;
    public double Volume = 0.0;
    public String State = "";
    public String GUIStage = "";
    public String TimeElapse = "";
    public Map<String, String> Gates = new HashMap<>();
    public Map<String, String> PushGates = new HashMap<>();
    public Map<String, String> PullGates = new HashMap<>();
    public Map<String, String> CollectionCriteriaMap = new HashMap<>();
    public Map<String, String> CurrentValue = new HashMap<>();
    public String gates = "";


    public JSONParse(String key, String device, String end){
        authKey = key;
        currentDevice = device;
        if(end == null){
            endPoint = "/status/";
        }
        if(currentDevice.equals("devices")){
            url = baseURL+currentDevice+"/";
        }else{
            url = baseURL+endPoint+currentDevice+"/";
        }
        jsonSetUP();
        setUpStatus();
    }

    public void jsonStart(){
        jsonSetUP();
        setUpStatus();
    }

    public void jsonSetUP(){
        HttpHandler httpHandler = new HttpHandler(authKey);
        String jsonStr = httpHandler.makeServiceCall(url);
        try {
            Log.d("Response from url: ", jsonStr);
            JSONObject jsonObject = new JSONObject(jsonStr);
            if (jsonStr != null && jsonObject.length() > 0) {
                if (jsonObject.has("devices")) {
                    JSONArray device = jsonObject.getJSONArray("devices");
                    address = new String[device.length()];
                    for (int i = 0; i < device.length(); i++) {
                        address[i] = device.getString(i);
                    }
                } else {
                    if (jsonObject.has("gates")) {
                        JSONObject gateObject = jsonObject.getJSONObject("gates");
                        if (gateObject.length() > 0) {
                            Iterator<String> keys = gateObject.keys();
                            while (keys.hasNext()) {
                                String keyValue = keys.next();
                                String valueString = gateObject.getString(keyValue);
                                Gates.put(keyValue, valueString);
                            }
                        }
                    }
                    if (jsonObject.has("push_gates")) {
                        JSONObject gateObject = jsonObject.getJSONObject("push_gates");
                        if (gateObject.length() > 0) {
                            Iterator<String> keys = gateObject.keys();
                            while (keys.hasNext()) {
                                String keyValue = keys.next();
                                String valueString = gateObject.getString(keyValue);
                                PushGates.put(keyValue, valueString);
                            }
                        }
                    }
                    if (jsonObject.has("pull_gates")) {
                        JSONObject gateObject = jsonObject.getJSONObject("pull_gates");
                        if (gateObject.length() > 0) {
                            Iterator<String> keys = gateObject.keys();
                            while (keys.hasNext()) {
                                String keyValue = keys.next();
                                String valueString = gateObject.getString(keyValue);
                                PullGates.put(keyValue, valueString);
                            }
                        }
                    }
                    if (jsonObject.has("collection_criteria")) {
                        JSONObject gateObject = jsonObject.getJSONObject("collection_criteria");
                        if (gateObject.length() > 0) {
                            Iterator<String> keys = gateObject.keys();
                            while (keys.hasNext()) {
                                String keyValue = keys.next();
                                String valueString = gateObject.getString(keyValue);
                                CollectionCriteriaMap.put(keyValue, valueString);
                            }
                        }
                    }
                    if (jsonObject.has("elapsed")) {
                        elapse = Integer.parseInt(jsonObject.getString("elapsed"));
                        CurrentValue.put("elapsed", Integer.toString(elapse));
                    }
                    if (jsonObject.has("gui_stage")) {
                        GUIStage = jsonObject.getString("gui_stage");
                    }
                    totalCellCount = Integer.parseInt(jsonObject.getString("total_cell_count"));
                    ATriggerCount = Integer.parseInt(jsonObject.getString("a_trigger_count"));
                    AImpedanceCount = Integer.parseInt(jsonObject.getString("a_impedance_count"));
                    AEfficiency = Double.parseDouble(jsonObject.getString("a_efficiency"));
                    CTriggerCount = Integer.parseInt(jsonObject.getString("c_trigger_count"));
                    CImpedanceCount = Integer.parseInt(jsonObject.getString("c_impedance_count"));
                    CEfficiency = Double.parseDouble(jsonObject.getString("c_efficiency"));
                    BImpedanceCount = Integer.parseInt(jsonObject.getString("b_impedance_count"));
                    Volume = Double.parseDouble(jsonObject.getString("volume"));
                    State = jsonObject.getString("state");
                    CurrentValue.put("total_cell_count", Integer.toString(totalCellCount));
                    CurrentValue.put("a_trigger_count", Integer.toString(ATriggerCount));
                    CurrentValue.put("c_trigger_count", Integer.toString(CTriggerCount));
                    CurrentValue.put("volume", Double.toString(Volume));

                }
            }

        } catch (JSONException e) {
            Log.d("Json parsing error: ", e.getMessage());
        }
    }

    private void setUpStatus(){
        int minutes = elapse/60;
        int seconds = elapse%60;
        TimeElapse =  String.format("%02d:%02d", minutes, seconds);
        if(GUIStage == null || GUIStage == ""){
            GUIStage = "NA";
        }
        Volume = towDecimals(Volume);
        if(Gates.size() == 0){
            gates = "     NONE";
        }
    }

    private double towDecimals(double d){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return Double.valueOf(decimalFormat.format(d));
    }

}
