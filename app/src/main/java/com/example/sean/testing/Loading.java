package com.example.sean.testing;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

/**
 * Created by Sean on 2/7/2017.
 */

public class Loading extends AppCompatActivity{
    private ImageView greenball;
    private ImageView blueball_1;
    private ImageView blueball_2;
    private Toolbar bar;
    private AnimatorSet animatorSet;
    private boolean isFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        final Thread welcome = new Thread(){
            public void run(){
                try{
                    super.run();
                    sleep(2000);
                }catch(Exception e){

                }finally {
                    {
                        Intent intent = new Intent(Loading.this, DeviceMain.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        };
        welcome.start();

        Runnable swapeImage_green =  new Runnable() {
            @Override
            public void run() {
                greenball.setImageResource(R.drawable.green);
            }
        };

        Runnable swapeImage_blue = new Runnable() {
            @Override
            public void run() {
                blueball_2.setImageResource(R.drawable.blue);
            }
        };

        blueball_1 = (ImageView)findViewById(R.id.blueball_1);
        start(blueball_1,515,0);
        greenball = (ImageView)findViewById(R.id.greenball);
        start(greenball, 295, 200);
        greenball.postDelayed(swapeImage_green, 130);
        blueball_2 = (ImageView)findViewById(R.id.blueball_2);
        start(blueball_2,75,400);
        blueball_2.postDelayed(swapeImage_blue, 620);
    }

    public void start(ImageView view, float num, int delay){
        ObjectAnimator move = ObjectAnimator.ofFloat(view, "translationY", num);
        move.setDuration(700);
        animatorSet = new AnimatorSet();
        animatorSet.setStartDelay(delay);
        animatorSet.play(move);
        animatorSet.start();
    }
}
